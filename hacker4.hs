hello_worlds n | n == 1 = putStrLn "Hello World"
               | otherwise = do
	                         putStrLn "Hello World"
				 hello_worlds (n-1)
  main = do
  n <- readLn :: IO Int
  hello_worlds n

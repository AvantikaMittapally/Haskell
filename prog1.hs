fibonacci :: [Int]
fibonacci = 1 : 1 : (next 1 1)
 where 
 next x y = sxy : (next y sxy)
 where
 sxy = x+y
 main = do 
 putStrLn $ show $ sum $ filter odd $ takeWhile (< 4000000) fibonacci

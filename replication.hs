f :: Int -> [Int] -> [Int]
f _ [] =[]
f n (x:xs) = [x| _ <- [1..n]] ++ f n xs
main :: IO ()
main = getContents >>=
          mapM_ print. (\(n:arr)  -> f n arr) .map read .words

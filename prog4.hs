main = do
let z = [x*y | x <- [100..999], y <- [100..999], y >= x]
print $ maximum $ filter (\x -> show x == reverse (show x)) z
